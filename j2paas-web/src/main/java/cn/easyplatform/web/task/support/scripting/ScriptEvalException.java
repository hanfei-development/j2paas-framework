/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.support.scripting;

import cn.easyplatform.EasyPlatformRuntimeException;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ScriptEvalException extends EasyPlatformRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int line = -1;


	public ScriptEvalException(String message, int line) {
		super(message);
		this.line = line;
	}

	public ScriptEvalException(String message, Throwable cause, int line) {
		super(message, cause);
		this.line = line;
	}

	public int getLine() {
		return line;
	}

}
