/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListReloadVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private Object[] fromKeys;

	private boolean memory;
	
	public ListReloadVo(String id, Object[] fromKeys,boolean memory) {
		this.id = id;
		this.fromKeys = fromKeys;
		this.memory = memory;
	}

	public String getId() {
		return id;
	}

	public Object[] getFromKeys() {
		return fromKeys;
	}

	/**
	 * @return the memory
	 */
	public boolean isMemory() {
		return memory;
	}

}
