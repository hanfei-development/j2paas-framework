/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.ZkExt;
import org.apache.commons.io.IOUtils;
import org.zkoss.pivot.Pivottable;
import org.zkoss.pivot.util.Exports;
import org.zkoss.pivot.util.PivotExportContext;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Filedownload;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PivottableExt extends Pivottable implements ZkExt {

    /**
     *
     */
    private static final long serialVersionUID = 6732496876422100223L;

    /**
     * 报表参数的id
     */
    private String _entity;

    /**
     * 自定义样式脚本
     */
    private String _cellStyle;

    /**
     * 在显示时是否要马上执行查询
     */
    private boolean _immediate = true;

    /**
     * 过滤表达式
     */
    private String _filter;

    /**
     * 控件中心
     */
    private String _control;

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    public String getEntity() {
        return _entity;
    }

    public void setEntity(String entityId) {
        this._entity = entityId;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    /**
     * @return the cellStyle
     */
    public String getCellStyle() {
        return _cellStyle;
    }

    /**
     * @param cellStyle the cellStyle to set
     */
    public void setCellStyle(String cellStyle) {
        this._cellStyle = cellStyle;
    }

    /**
     * @return the control
     */
    public String getControl() {
        return _control;
    }

    /**
     * @param control the control to set
     */
    public void setControl(String control) {
        this._control = control;
    }

    public void export(String type) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            if ("csv".equalsIgnoreCase(type)) {
                PivotExportContext context = Exports.getExportContext(this, true, null);
                Exports.exportCSV(out, context);
                Filedownload.save(out.toByteArray(), "text/csv", "pivot.csv");
            } else {
                PivotExportContext context = Exports.getExportContext(this, false, null);
                if ("xlsx".equalsIgnoreCase(type)) {
                    Exports.exportExcel(out, "xlsx", context, null);
                    Filedownload.save(out.toByteArray(), "application/vnd.ms-excel", "pivot.xlsx");
                } else if ("xls".equalsIgnoreCase(type)) {
                    Exports.exportExcel(out, "xls", context, null);
                    Filedownload.save(out.toByteArray(), "application/vnd.ms-excel", "pivot.xls");
                }
            }
        } catch (IOException e) {
            Clients.alert(Labels.getLabel(e.getMessage()));
        } finally {
            IOUtils.closeQuietly(out);
        }
    }
}
