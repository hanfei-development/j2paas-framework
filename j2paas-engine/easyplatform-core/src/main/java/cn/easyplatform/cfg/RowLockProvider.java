/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.entities.beans.table.TableBean;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface RowLockProvider {

    /**
     * 锁定表记录
     *
     * @param tb
     * @param record
     */
    String lock(TableBean tb, RecordContext record);

    /**
     * @param tableId
     * @param keys
     */
    String lock(String tableId, Object... keys);

    /**
     * @param tableId
     * @param keys
     */
    void unlock(String tableId, Object... keys);

    /**
     * 解锁指定的任务id
     *
     * @param id
     * @param layer
     */
    void unlock(String id, int layer);

    /**
     * @param tableId
     * @param keys
     */
    void exclusiveLock(String tableId, Object... keys);

    /**
     * @param key
     */
    void unlock(String key);

    /**
     * 清空用户锁记录
     */
    void clear();

    /**
     * 清空用户锁记录,由后端调用
     */
    void clear(UserDo user);


    class LockInfo implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        private String userId;

        private String taskId;

        private int layer;

        private String key;

        private String funId;

        public LockInfo(String userId, String taskId, int layer, String key) {
            this.userId = userId;
            this.taskId = taskId;
            this.layer = layer;
            this.key = key;
        }

        public String getUserId() {
            return userId;
        }

        public String getTaskId() {
            return taskId;
        }

        public int getLayer() {
            return layer;
        }

        public String getKey() {
            return key;
        }

        public String getFunId() {
            return funId;
        }

        public LockInfo setFunId(String funId) {
            this.funId = funId;
            return this;
        }
    }
}
