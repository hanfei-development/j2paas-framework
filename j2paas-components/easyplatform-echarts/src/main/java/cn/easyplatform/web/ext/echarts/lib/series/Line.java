/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.AreaStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Line extends Series {
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer polarIndex;
    private Object symbol;
    private Object symbolSize;
    private Object symbolRotate;
    private Object symbolOffset;
    private Boolean showSymbol;
    private Boolean showAllSymbol;
    private Boolean legendHoverLink;
    private Object stack;
    private Boolean connectNulls;
    private Boolean clipOverflow;
    private Boolean step;
    private LineStyle lineStyle;
    private AreaStyle areaStyle;
    private Boolean smooth;
    private String smoothMonotone;
    private String sampling;
    private Boolean hoverAnimation;
    private Boolean symbolKeepAspect;

    public Line() {
        type="line";
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public Line symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public Boolean hoverAnimation() {
        return hoverAnimation;
    }

    public Line hoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Line coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Line xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Line yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Integer polarIndex() {
        return polarIndex;
    }

    public Line polarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public Line symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public Line symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Object symbolRotate() {
        return symbolRotate;
    }

    public Line symbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return symbolOffset;
    }

    public Line symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public Boolean showSymbol() {
        return showSymbol;
    }

    public Line showSymbol(Boolean showSymbol) {
        this.showSymbol = showSymbol;
        return this;
    }

    public Boolean showAllSymbol() {
        return showAllSymbol;
    }

    public Line showAllSymbol(Boolean showAllSymbol) {
        this.showAllSymbol = showAllSymbol;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Line legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Object stack() {
        return stack;
    }

    public Line stack(Object stack) {
        this.stack = stack;
        return this;
    }

    public Boolean connectNulls() {
        return connectNulls;
    }

    public Line connectNulls(Boolean connectNulls) {
        this.connectNulls = connectNulls;
        return this;
    }

    public Boolean clipOverflow() {
        return clipOverflow;
    }

    public Line clipOverflow(Boolean clipOverflow) {
        this.clipOverflow = clipOverflow;
        return this;
    }

    public Boolean step() {
        return step;
    }

    public Line step(Boolean step) {
        this.step = step;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return lineStyle;
    }

    public Line lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public AreaStyle areaStyle() {
        if (areaStyle == null)
            areaStyle = new AreaStyle();
        return areaStyle;
    }

    public Line areaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
        return this;
    }

    public Boolean smooth() {
        return smooth;
    }

    public Line smooth(Boolean smooth) {
        this.smooth = smooth;
        return this;
    }

    public String smoothMonotone() {
        return smoothMonotone;
    }

    public Line smoothMonotone(String smoothMonotone) {
        this.smoothMonotone = smoothMonotone;
        return this;
    }

    public String sampling() {
        return sampling;
    }

    public Line sampling(String sampling) {
        this.sampling = sampling;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getPolarIndex() {
        return polarIndex;
    }

    public void setPolarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Object getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Boolean getShowSymbol() {
        return showSymbol;
    }

    public void setShowSymbol(Boolean showSymbol) {
        this.showSymbol = showSymbol;
    }

    public Boolean getShowAllSymbol() {
        return showAllSymbol;
    }

    public void setShowAllSymbol(Boolean showAllSymbol) {
        this.showAllSymbol = showAllSymbol;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Object getStack() {
        return stack;
    }

    public void setStack(Object stack) {
        this.stack = stack;
    }

    public Boolean getConnectNulls() {
        return connectNulls;
    }

    public void setConnectNulls(Boolean connectNulls) {
        this.connectNulls = connectNulls;
    }

    public Boolean getClipOverflow() {
        return clipOverflow;
    }

    public void setClipOverflow(Boolean clipOverflow) {
        this.clipOverflow = clipOverflow;
    }

    public Boolean getStep() {
        return step;
    }

    public void setStep(Boolean step) {
        this.step = step;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public AreaStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
    }

    public Boolean getSmooth() {
        return smooth;
    }

    public void setSmooth(Boolean smooth) {
        this.smooth = smooth;
    }

    public String getSmoothMonotone() {
        return smoothMonotone;
    }

    public void setSmoothMonotone(String smoothMonotone) {
        this.smoothMonotone = smoothMonotone;
    }

    public String getSampling() {
        return sampling;
    }

    public void setSampling(String sampling) {
        this.sampling = sampling;
    }
}
