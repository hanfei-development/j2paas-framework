/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.zoom;

import cn.easyplatform.web.ext.echarts.lib.type.DataZoomType;
import cn.easyplatform.web.ext.echarts.lib.type.FilterMode;
import cn.easyplatform.web.ext.echarts.lib.type.Orient;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据区域缩放。与toolbox.feature.dataZoom同步，仅对直角坐标系图表有效
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class DataZoom implements Serializable {

    private Boolean show;
    /**
     * 类型
     * @see DataZoomType
     */
    private String type;
    /**
     * 当不指定时默认控制所有横向类目，可通过数组指定多个需要控制的横向类目坐标轴Index，仅一个时可直接为数字
     */
    private Object xAxisIndex;
    /**
     * 当不指定时默认控制所有纵向类目，可通过数组指定多个需要控制的纵向类目坐标轴Index，仅一个时可直接为数字
     */
    private Object yAxisIndex;
    /**
     * 设置 dataZoom-inside 组件控制的 角度轴
     */
    private Object angleAxisIndex;
    /**
     * 设置 dataZoom-inside 组件控制的 半径轴
     */
    private Object radiusAxisIndex;
    /**
     * dataZoom 的运行原理是通过 数据过滤 来达到 数据窗口缩放 的效果
     * @see FilterMode
     */
    private String filterMode;
    /**
     * 数据缩放，选择起始比例，默认为0（%），从首个数据起选择
     */
    private Integer start;
    /**
     * 数据缩放，选择结束比例，默认为100（%），到最后一个数据选择结束
     */
    private Integer end;
    /**
     * 数据窗口范围的起始数值。如果设置了 dataZoom-slider.start 则 startValue 失效
     */
    private Object startValue;
    /**
     * 数据窗口范围的结束数值。如果设置了 dataZoom-slider.end 则 endValue 失效
     */
    private Object endValue;
    /**
     * 布局方式，默认为水平布局，可选为：'horizontal' | 'vertical'
     */
    private Orient orient;
    /**
     * 数据缩放锁，默认为false，当设置为true时选择区域不能伸缩，即(end - start)值保持不变，仅能做数据漫游
     */
    private Boolean zoomLock;
    /**
     * 设置触发视图刷新的频率。单位为毫秒（ms）。一般不需要更改这个值
     */
    private Integer throttle;
    /**
     * 形式为：[rangeModeForStart, rangeModeForEnd]。
     *
     * 例如 rangeMode: ['value', 'percent']，表示 start 值取绝对数值，end 取百分比
     */
    private String[] rangeMode;
    /**
     * 用于限制窗口大小的最小值（百分比值），取值范围是 0 ~ 100
     */
    private Integer minSpan;
    /**
     * 用于限制窗口大小的最大值（百分比值），取值范围是 0 ~ 100。
     */
    private Integer maxSpan;
    /**
     * 用于限制窗口大小的最小值（实际数值）。
     *
     * 如在时间轴上可以设置为：3600 * 24 * 1000 * 5 表示 5 天。 在类目轴上可以设置为 5 表示 5 个类目
     */
    private Object minValueSpan;
    /**
     * 用于限制窗口大小的最大值（实际数值）。
     *
     * 如在时间轴上可以设置为：3600 * 24 * 1000 * 5 表示 5 天。 在类目轴上可以设置为 5 表示 5 个类目。
     */
    private Object maxValueSpan;

    public Object maxValueSpan() {
        return this.maxValueSpan;
    }

    public DataZoom maxValueSpan(Object maxValueSpan) {
        this.maxValueSpan = maxValueSpan;
        return this;
    }

    public Object minValueSpan() {
        return this.minValueSpan;
    }

    public DataZoom minValueSpan(Object minValueSpan) {
        this.minValueSpan = minValueSpan;
        return this;
    }

    public Integer maxSpan() {
        return this.minSpan;
    }

    public DataZoom maxSpan(Integer maxSpan) {
        this.maxSpan = maxSpan;
        return this;
    }

    public Integer minSpan() {
        return this.minSpan;
    }

    public DataZoom minSpan(Integer minSpan) {
        this.minSpan = minSpan;
        return this;
    }

    public String[] rangeMode() {
        return this.rangeMode;
    }

    public DataZoom rangeMode(String[] rangeMode) {
        this.rangeMode = rangeMode;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public DataZoom show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object startValue() {
        return this.startValue;
    }

    public DataZoom startValue(Object startValue) {
        this.startValue = startValue;
        return this;
    }

    public DataZoom startValue(String startValue) {
        this.startValue = startValue;
        return this;
    }

    public DataZoom startValue(Integer startValue) {
        this.startValue = startValue;
        return this;
    }

    public DataZoom startValue(Date startValue) {
        this.startValue = startValue;
        return this;
    }

    public Object endValue() {
        return this.endValue;
    }

    public DataZoom endValue(Object endValue) {
        this.endValue = endValue;
        return this;
    }

    public DataZoom endValue(Integer endValue) {
        this.endValue = endValue;
        return this;
    }

    public DataZoom endValue(String endValue) {
        this.endValue = endValue;
        return this;
    }

    public DataZoom endValue(Date endValue) {
        this.endValue = endValue;
        return this;
    }

    public Object getStartValue() {
        return startValue;
    }

    public void setStartValue(Object startValue) {
        this.startValue = startValue;
    }

    public Object getEndValue() {
        return endValue;
    }

    public void setEndValue(Object endValue) {
        this.endValue = endValue;
    }

    public String type() {
        return this.type;
    }

    public DataZoom type(String type) {
        this.type = type;
        return this;
    }

    public Object angleAxisIndex() {
        return this.angleAxisIndex;
    }

    public DataZoom angleAxisIndex(Object angleAxisIndex) {
        this.angleAxisIndex = angleAxisIndex;
        return this;
    }

    public DataZoom angleAxisIndex(Integer angleAxisIndex) {
        this.angleAxisIndex = angleAxisIndex;
        return this;
    }

    public DataZoom angleAxisIndex(Integer... angleAxisIndex) {
        this.angleAxisIndex = angleAxisIndex;
        return this;
    }

    public Object radiusAxisIndex() {
        return this.radiusAxisIndex;
    }

    public DataZoom radiusAxisIndex(Object radiusAxisIndex) {
        this.radiusAxisIndex = radiusAxisIndex;
        return this;
    }

    public DataZoom radiusAxisIndex(Integer radiusAxisIndex) {
        this.radiusAxisIndex = radiusAxisIndex;
        return this;
    }

    public DataZoom radiusAxisIndex(Integer... radiusAxisIndex) {
        this.radiusAxisIndex = radiusAxisIndex;
        return this;
    }

    public String filterMode() {
        return this.filterMode;
    }

    public DataZoom filterMode(String filterMode) {
        this.filterMode = filterMode;
        return this;
    }

    public Integer throttle() {
        return this.throttle;
    }

    public DataZoom throttle(Integer throttle) {
        this.throttle = throttle;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getAngleAxisIndex() {
        return angleAxisIndex;
    }

    public void setAngleAxisIndex(Object angleAxisIndex) {
        this.angleAxisIndex = angleAxisIndex;
    }

    public Object getRadiusAxisIndex() {
        return radiusAxisIndex;
    }

    public void setRadiusAxisIndex(Object radiusAxisIndex) {
        this.radiusAxisIndex = radiusAxisIndex;
    }

    public String getFilterMode() {
        return filterMode;
    }

    public void setFilterMode(String filterMode) {
        this.filterMode = filterMode;
    }

    public Integer getThrottle() {
        return throttle;
    }

    public void setThrottle(Integer throttle) {
        this.throttle = throttle;
    }

    /**
     * 获取orient值
     */
    public Orient orient() {
        return this.orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public DataZoom orient(Orient orient) {
        this.orient = orient;
        return this;
    }

    /**
     * 获取xAxisIndex值
     */
    public Object xAxisIndex() {
        return this.xAxisIndex;
    }

    /**
     * 设置xAxisIndex值
     *
     * @param xAxisIndex
     */
    public DataZoom xAxisIndex(Object xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    /**
     * 获取yAxisIndex值
     */
    public Object yAxisIndex() {
        return this.yAxisIndex;
    }

    /**
     * 设置yAxisIndex值
     *
     * @param yAxisIndex
     */
    public DataZoom yAxisIndex(Object yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    /**
     * 获取start值
     */
    public Integer start() {
        return this.start;
    }

    /**
     * 设置start值
     *
     * @param start
     */
    public DataZoom start(Integer start) {
        this.start = start;
        return this;
    }

    /**
     * 获取end值
     */
    public Integer end() {
        return this.end;
    }

    /**
     * 设置end值
     *
     * @param end
     */
    public DataZoom end(Integer end) {
        this.end = end;
        return this;
    }

    /**
     * 获取zoomLock值
     */
    public Boolean zoomLock() {
        return this.zoomLock;
    }

    /**
     * 设置zoomLock值
     *
     * @param zoomLock
     */
    public DataZoom zoomLock(Boolean zoomLock) {
        this.zoomLock = zoomLock;
        return this;
    }

    /**
     * 获取orient值
     */
    public Orient getOrient() {
        return orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public void setOrient(Orient orient) {
        this.orient = orient;
    }

    /**
     * 获取xAxisIndex值
     */
    public Object getxAxisIndex() {
        return xAxisIndex;
    }

    /**
     * 设置xAxisIndex值
     *
     * @param xAxisIndex
     */
    public void setxAxisIndex(Object xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    /**
     * 获取yAxisIndex值
     */
    public Object getyAxisIndex() {
        return yAxisIndex;
    }

    /**
     * 设置yAxisIndex值
     *
     * @param yAxisIndex
     */
    public void setyAxisIndex(Object yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    /**
     * 获取start值
     */
    public Integer getStart() {
        return start;
    }

    /**
     * 设置start值
     *
     * @param start
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * 获取end值
     */
    public Integer getEnd() {
        return end;
    }

    /**
     * 设置end值
     *
     * @param end
     */
    public void setEnd(Integer end) {
        this.end = end;
    }

    /**
     * 获取zoomLock值
     */
    public Boolean getZoomLock() {
        return zoomLock;
    }

    /**
     * 设置zoomLock值
     *
     * @param zoomLock
     */
    public void setZoomLock(Boolean zoomLock) {
        this.zoomLock = zoomLock;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public String[] getRangeMode() {
        return rangeMode;
    }

    public void setRangeMode(String[] rangeMode) {
        this.rangeMode = rangeMode;
    }

    public Integer getMinSpan() {
        return minSpan;
    }

    public void setMinSpan(Integer minSpan) {
        this.minSpan = minSpan;
    }

    public Integer getMaxSpan() {
        return maxSpan;
    }

    public void setMaxSpan(Integer maxSpan) {
        this.maxSpan = maxSpan;
    }

    public Object getMinValueSpan() {
        return minValueSpan;
    }

    public void setMinValueSpan(Object minValueSpan) {
        this.minValueSpan = minValueSpan;
    }

    public Object getMaxValueSpan() {
        return maxValueSpan;
    }

    public void setMaxValueSpan(Object maxValueSpan) {
        this.maxValueSpan = maxValueSpan;
    }
}
