/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;
import cn.easyplatform.web.ext.echarts.lib.support.ScaleLimit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Geo extends Point {
    /**
     * 是否显示地理坐标系组件。
     */
    private Boolean show;
    /**
     * 地图类型
     */
    private String map;
    /**
     * 是否开启鼠标缩放和平移漫游。默认不开启。如果只想要开启缩放或者平移，可以设置成 'scale' 或者 'move'。设置成 true 为都开启
     */
    private Object roam;
    /**
     * 当前视角的中心点，用经纬度表示
     */
    private Object center;
    /**
     * 这个参数用于 scale 地图的长宽比
     */
    private Object aspectScale;
    /**
     * 当前视角的缩放比例
     */
    private Object zoom;
    /**
     * 滚轮缩放的极限控制，通过min, max最小和最大的缩放值，默认的缩放为1
     */
    private ScaleLimit scaleLimit;
    /**
     * 自定义地区的名称映射
     */
    private Map<String, String> nameMap;
    /**
     * 选中模式，表示是否支持多个选中，默认关闭，支持布尔值和字符串，字符串取值可选'single'表示单选，或者'multiple'表示多选
     */
    private Object selectedMode;
    /**
     * 图形上的文本标签，可用于说明图形的一些数据信息，比如值，名称等
     */
    private ItemStyle label;
    /**
     * 地图区域的多边形 图形样式，有 normal 和 emphasis 两个状态。normal 是图形在默认状态下的样式；emphasis 是图形在高亮状态下的样式，比如在鼠标悬浮或者图例联动高亮时
     */
    private ItemStyle itemStyle;
    /**
     * layoutCenter 和 layoutSize 提供了除了 left/right/top/bottom/width/height 之外的布局手段
     */
    private Object layoutCenter;
    /**
     * 地图的大小，见 layoutCenter。支持相对于屏幕宽高的百分比或者绝对的像素大小
     */
    private Object layoutSize;
    /**
     * 在地图中对特定的区域配置样式
     */
    private List<Regions> regions;

    /**
     * 图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件
     */
    private Boolean silent;

    public List<Regions> regions() {
        if (regions == null)
            regions = new ArrayList<Regions>();
        return regions;
    }

    public Geo regions(Regions region) {
        if (regions == null)
            regions = new ArrayList<Regions>();
        regions.add(region);
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Geo silen(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public Object layoutSize() {
        return this.layoutSize;
    }

    public Geo layoutSize(Object layoutSize) {
        this.layoutSize = layoutSize;
        return this;
    }

    public Object layoutCenter() {
        return this.layoutCenter;
    }

    public Geo layoutCenter(Object layoutCenter) {
        this.layoutCenter = layoutCenter;
        return this;
    }

    public Object selectedMode() {
        return selectedMode;
    }

    public Geo selectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
        return this;
    }

    public ScaleLimit scaleLimit() {
        return scaleLimit;
    }

    public Geo scaleLimit(ScaleLimit scaleLimit) {
        this.scaleLimit = scaleLimit;
        return this;
    }

    public Object zoom() {
        return zoom;
    }

    public Geo zoom(Object zoom) {
        this.zoom = zoom;
        return this;
    }

    public Object aspectScale() {
        return aspectScale;
    }

    public Geo aspectScale(Object aspectScale) {
        this.aspectScale = aspectScale;
        return this;
    }

    public Object center() {
        return center;
    }

    public Geo center(Object center) {
        this.center = center;
        return this;
    }

    public Map<String, String> nameMap() {
        if (nameMap == null)
            nameMap = new HashMap<String, String>();
        return nameMap;
    }

    public Geo nameMap(String name, String map) {
        if (nameMap == null)
            nameMap = new HashMap<String, String>();
        nameMap.put(name, map);
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public Geo show(Boolean show) {
        this.show = show;
        return this;
    }

    public String map() {
        return this.map;
    }

    public Geo map(String map) {
        this.map = map;
        return this;
    }

    public Object roam() {
        return this.roam;
    }

    public Geo roam(Object roam) {
        this.roam = roam;
        return this;
    }

    public ItemStyle label() {
        if (this.label == null) {
            this.label = new ItemStyle();
        }
        return this.label;
    }

    public Geo label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (this.itemStyle == null) {
            this.itemStyle = new ItemStyle();
        }
        return this.itemStyle;
    }

    public Geo itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public Object getRoam() {
        return roam;
    }

    public void setRoam(Object roam) {
        this.roam = roam;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getCenter() {
        return center;
    }

    public void setCenter(Object center) {
        this.center = center;
    }

    public Object getAspectScale() {
        return aspectScale;
    }

    public void setAspectScale(Object aspectScale) {
        this.aspectScale = aspectScale;
    }

    public Object getZoom() {
        return zoom;
    }

    public void setZoom(Object zoom) {
        this.zoom = zoom;
    }

    public ScaleLimit getScaleLimit() {
        return scaleLimit;
    }

    public void setScaleLimit(ScaleLimit scaleLimit) {
        this.scaleLimit = scaleLimit;
    }

    public Map<String, String> getNameMap() {
        return nameMap;
    }

    public void setNameMap(Map<String, String> nameMap) {
        this.nameMap = nameMap;
    }

    public Object getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;
    }

    public Object getLayoutCenter() {
        return layoutCenter;
    }

    public void setLayoutCenter(Object layoutCenter) {
        this.layoutCenter = layoutCenter;
    }

    public Object getLayoutSize() {
        return layoutSize;
    }

    public void setLayoutSize(Object layoutSize) {
        this.layoutSize = layoutSize;
    }

    public List<Regions> getRegions() {
        return regions;
    }

    public void setRegions(List<Regions> regions) {
        this.regions = regions;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public static class Regions {
        /**
         * 地图区域的名称，例如 '广东'，'浙江'
         */
        private String name;
        /**
         * 该区域是否选中
         */
        private Boolean selected;
        /**
         * 该区域的多边形样式设置
         */
        private ItemStyle itemStyle;
        /**
         * 该区域的标签样式设置
         */
        private LabelStyle label;
        /**
         * 高亮的样式。
         */
        private Emphasis emphasis;

        public Emphasis emphasis() {
            if (emphasis == null)
                emphasis = new Emphasis();
            return emphasis;
        }

        public Regions emphasis(Emphasis emphasis) {
            this.emphasis = emphasis;
            return this;
        }

        public String name() {
            return name;
        }

        public Regions name(String name) {
            this.name = name;
            return this;
        }

        public Boolean selected() {
            return selected;
        }

        public Regions selected(Boolean selected) {
            this.selected = selected;
            return this;
        }

        public ItemStyle itemStyle() {
            if (itemStyle == null)
                itemStyle = new ItemStyle();
            return itemStyle;
        }

        public Regions itemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
            return this;
        }

        public LabelStyle label() {
            if (label == null)
                label = new LabelStyle();
            return label;
        }

        public Regions label(LabelStyle label) {
            this.label = label;
            return this;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        public ItemStyle getItemStyle() {
            return itemStyle;
        }

        public void setItemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
        }

        public LabelStyle getLabel() {
            return label;
        }

        public void setLabel(LabelStyle label) {
            this.label = label;
        }

        public Emphasis getEmphasis() {
            return emphasis;
        }

        public void setEmphasis(Emphasis emphasis) {
            this.emphasis = emphasis;
        }
    }
}
