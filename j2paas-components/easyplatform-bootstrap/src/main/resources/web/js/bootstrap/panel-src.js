zk.afterLoad('zul.wnd', function () {
    var _panel = {},
        _panelMolds = {};

    zk.override(zul.wnd.Panel.molds, _panelMolds, {
        'bs': zul.wnd.Panel.molds['default']
    });

    zk.override(zul.wnd.Panel.prototype, _panel, {
        _inBSMold: function () {
            return this._mold == 'bs';
        },
        getSclass: function () {
            if (this._inBSMold()) {
                return '';
            } else
                return _panel.getSclass.apply(this, arguments);
        },
        getZclass: function () {
            if (this._inBSMold())
                return 'card';
            return _panel.getZclass.apply(this, arguments);
        },
        $s: function (subclass) {
            if (this._inBSMold()) {
                switch (subclass) {
                    case 'head':
                        subclass = this._sclass ? 'header ' + this._sclass : 'header';
                        break;
                    case 'header':
                        return 'font-weight-bold';
                }
            }
            return _panel.$s.apply(this, arguments);
        }
    });

    var _panelchildren = {};

    zk.override(zul.wnd.Panelchildren.prototype, _panelchildren, {
        _inBSMold: function () {
            return this.parent && this.parent._inBSMold();
        },
        bind_: function () {
            _panelchildren.bind_.apply(this, arguments);
            if (this._inBSMold())
                jq(this).removeClass('card-body');
        },
        getZclass: function () {
            if (this._inBSMold())
                return this._zclass != null ? this._zclass : 'card-body';
            return _panelchildren.getZclass.apply(this, arguments);
        },
        $s: function (subclass) {
            if (this._inBSMold())
                return '';
            return _panelchildren.$s.apply(this, arguments);
        }
    });

});