/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PictorialBar extends Series {
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Boolean legendHoverLink;
    private Object barWidth;
    private Object barMaxWidth;
    private Object barMinHeight;
    private Object barGap;
    private Object barCategoryGap;
    private Object symbol;
    private Object symbolSize;
    private Object symbolPosition;
    private Object symbolRoate;
    private Object symbolRepeat;
    private Object symbolOffset;
    private String symbolRepeatDirection;
    private Object symbolMargin;
    private Boolean symbolClip;
    private Object symbolBoundingData;
    private Object symbolPatternSize;
    private Object hoverAnimation;

    public PictorialBar() {
        this.type="pictorialBar";
    }

    public Object symbolPosition() {
        return symbolPosition;
    }

    public PictorialBar symbolPosition(Object symbolPosition) {
        this.symbolPosition = symbolPosition;
        return this;
    }

    public Object symbolRepeat() {
        return symbolRepeat;
    }

    public PictorialBar symbolRepeat(Object symbolRepeat) {
        this.symbolRepeat = symbolRepeat;
        return this;
    }

    public String symbolRepeatDirection() {
        return symbolRepeatDirection;
    }

    public PictorialBar symbolRepeatDirection(String symbolRepeatDirection) {
        this.symbolRepeatDirection = symbolRepeatDirection;
        return this;
    }

    public Object symbolMargin() {
        return symbolMargin;
    }

    public PictorialBar symbolMargin(Object symbolMargin) {
        this.symbolMargin = symbolMargin;
        return this;
    }

    public Boolean symbolClip() {
        return symbolClip;
    }

    public PictorialBar symbolClip(Boolean symbolClip) {
        this.symbolClip = symbolClip;
        return this;
    }

    public Object symbolBoundingData() {
        return symbolBoundingData;
    }

    public PictorialBar symbolBoundingData(Object symbolBoundingData) {
        this.symbolBoundingData = symbolBoundingData;
        return this;
    }

    public Object symbolPatternSize() {
        return symbolPatternSize;
    }

    public PictorialBar symbolPatternSize(Object symbolPatternSize) {
        this.symbolPatternSize = symbolPatternSize;
        return this;
    }

    public Object hoverAnimation() {
        return hoverAnimation;
    }

    public PictorialBar hoverAnimation(Object hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public Object coordinateSystem() {
        return coordinateSystem;
    }

    public PictorialBar coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Object xAxisIndex() {
        return xAxisIndex;
    }

    public PictorialBar xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Object yAxisIndex() {
        return yAxisIndex;
    }

    public PictorialBar yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public PictorialBar legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Object barWidth() {
        return barWidth;
    }

    public PictorialBar barWidth(Object barWidth) {
        this.barWidth = barWidth;
        return this;
    }

    public Object barMaxWidth() {
        return barMaxWidth;
    }

    public PictorialBar barMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
        return this;
    }

    public Object barMinHeight() {
        return barMinHeight;
    }

    public PictorialBar barMinHeight(Object barMinHeight) {
        this.barMinHeight = barMinHeight;
        return this;
    }

    public Object barGap() {
        return barGap;
    }

    public PictorialBar barGap(Object barGap) {
        this.barGap = barGap;
        return this;
    }

    public Object barCategoryGap() {
        return barCategoryGap;
    }

    public PictorialBar barCategoryGap(Object barCategoryGap) {
        this.barCategoryGap = barCategoryGap;
        return this;
    }

    public Object symbol() {
        return this.symbol;
    }

    public PictorialBar symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public PictorialBar symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Object symbolRoate() {
        return this.symbolRoate;
    }

    public PictorialBar symbolRoate(Object symbolRoate) {
        this.symbolRoate = symbolRoate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public PictorialBar symbolOffset(Object value) {
        if (value instanceof String)
            this.symbolOffset = ((String) value).split(",");
        else
            this.symbolOffset = symbolOffset;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Object getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(Object barWidth) {
        this.barWidth = barWidth;
    }

    public Object getBarMaxWidth() {
        return barMaxWidth;
    }

    public void setBarMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
    }

    public Object getBarMinHeight() {
        return barMinHeight;
    }

    public void setBarMinHeight(Object barMinHeight) {
        this.barMinHeight = barMinHeight;
    }

    public Object getBarGap() {
        return barGap;
    }

    public void setBarGap(Object barGap) {
        this.barGap = barGap;
    }

    public Object getBarCategoryGap() {
        return barCategoryGap;
    }

    public void setBarCategoryGap(Object barCategoryGap) {
        this.barCategoryGap = barCategoryGap;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Object getSymbolPosition() {
        return symbolPosition;
    }

    public void setSymbolPosition(Object symbolPosition) {
        this.symbolPosition = symbolPosition;
    }

    public Object getSymbolRoate() {
        return symbolRoate;
    }

    public void setSymbolRoate(Object symbolRoate) {
        this.symbolRoate = symbolRoate;
    }

    public Object getSymbolRepeat() {
        return symbolRepeat;
    }

    public void setSymbolRepeat(Object symbolRepeat) {
        this.symbolRepeat = symbolRepeat;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public String getSymbolRepeatDirection() {
        return symbolRepeatDirection;
    }

    public void setSymbolRepeatDirection(String symbolRepeatDirection) {
        this.symbolRepeatDirection = symbolRepeatDirection;
    }

    public Object getSymbolMargin() {
        return symbolMargin;
    }

    public void setSymbolMargin(Object symbolMargin) {
        this.symbolMargin = symbolMargin;
    }

    public Boolean getSymbolClip() {
        return symbolClip;
    }

    public void setSymbolClip(Boolean symbolClip) {
        this.symbolClip = symbolClip;
    }

    public Object getSymbolBoundingData() {
        return symbolBoundingData;
    }

    public void setSymbolBoundingData(Object symbolBoundingData) {
        this.symbolBoundingData = symbolBoundingData;
    }

    public Object getSymbolPatternSize() {
        return symbolPatternSize;
    }

    public void setSymbolPatternSize(Object symbolPatternSize) {
        this.symbolPatternSize = symbolPatternSize;
    }

    public Object getHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(Object hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }
}
