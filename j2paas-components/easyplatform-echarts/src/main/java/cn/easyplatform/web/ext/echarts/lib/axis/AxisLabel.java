/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;


import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AxisLabel extends TextStyle implements Serializable {
    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * 挑选间隔，默认为'auto'，可选为：'auto'（自动隐藏显示不下的） | 0（全部显示） | {number}
     */
    private Object interval;
    /**
     * 小标记是否显示为在grid内部，默认在外部
     */
    private Boolean inside;
    /**
     * rotate : 旋转角度，默认为0，不旋转，正值为逆时针，负值为顺时针，可选为：-90 ~ 90
     */
    private Integer rotate;
    /**
     * 坐标轴文本标签与坐标轴的间距
     */
    private Integer margin;
    /**
     * 间隔名称格式器：{string}（Template） | {Function}
     */
    private Object formatter;

    /**
     * 文字块的内边距
     */
    private Object padding;

    public Object padding() {
        return this.padding;
    }

    public AxisLabel padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public AxisLabel show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object interval() {
        return this.interval;
    }

    public AxisLabel interval(Object interval) {
        this.interval = interval;
        return this;
    }

    public Boolean inside() {
        return this.inside;
    }

    public AxisLabel inside(Boolean inside) {
        this.inside = inside;
        return this;
    }

    public Integer rotate() {
        return this.rotate;
    }

    public AxisLabel rotate(Integer rotate) {
        this.rotate = rotate;
        return this;
    }

    public Integer margin() {
        return this.margin;
    }

    public AxisLabel margin(Integer margin) {
        this.margin = margin;
        return this;
    }

    public Object formatter() {
        return this.formatter;
    }

    public AxisLabel formatter(Object formatter) {
        this.formatter = formatter;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getInterval() {
        return interval;
    }

    public void setInterval(Object interval) {
        this.interval = interval;
    }

    public Boolean getInside() {
        return inside;
    }

    public void setInside(Boolean inside) {
        this.inside = inside;
    }

    public Integer getRotate() {
        return rotate;
    }

    public void setRotate(Integer rotate) {
        this.rotate = rotate;
    }

    public Integer getMargin() {
        return margin;
    }

    public void setMargin(Integer margin) {
        this.margin = margin;
    }

    public Object getFormatter() {
        return formatter;
    }

    public void setFormatter(Object formatter) {
        this.formatter = formatter;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }
}
