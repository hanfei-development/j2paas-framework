/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public enum EntityType {

    TASK("Task"),//可执行的功能

    PAGE("Page"),//页面

    DATALIST("Datalist"),//数据列表

    BPM("Bpm"),//业务流程

    LOGIC("Logic"),//逻辑

    REPORT("Report"),//报表

    RESOURCE("Resource"),//资源

    TABLE("Table"),//数据表

    BATCH("Batch"),//批处理

    PROJECT("Project"),//项目

    DATASOURCE("Datasource"),//数据源

    API("Api");//数据源

    String name;

    private EntityType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
