/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages;

import cn.easyplatform.type.IRequestMessage;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractRequestMessage<T> implements IRequestMessage<T>,
        Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2101390032635602127L;

    /**
     * 会话id
     */
    private Serializable sessionId;

    /**
     * 任务id
     */
    private String id;

    /**
     * 信息载体
     */
    private T body;

    /**
     * @param id
     * @param body
     */
    public AbstractRequestMessage(String id, T body) {
        this.id = id;
        this.body = body;
    }

    /**
     * @param body
     */
    public AbstractRequestMessage(T body) {
        this.body = body;
    }

    @Override
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public T getBody() {
        return body;
    }

    @Override
    public Serializable getSessionId() {
        return sessionId;
    }

    @Override
    public void setSessionId(Serializable sessionId){
        this.sessionId = sessionId;
    }
}
