{
    backgroundColor: '#161627',
    title: {
        text: 'AQI - 雷达图',
        left: 'center',
        textStyle: {
            color: '#eee'
        }
    },
    legend: {
        bottom: 5,
            data: ['北京', '上海', '广州'],
            itemGap: 20,
            textStyle: {
            color: '#fff',
                fontSize: 14
        },
        selectedMode: 'single'
    },
    // visualMap: {
    //     show: true,
    //     min: 0,
    //     max: 20,
    //     dimension: 6,
    //     inRange: {
    //         colorLightness: [0.5, 0.8]
    //     }
    // },
    radar: {
        indicator: [
            {name: 'AQI', max: 300},
            {name: 'PM2.5', max: 250},
            {name: 'PM10', max: 300},
            {name: 'CO', max: 5},
            {name: 'NO2', max: 200},
            {name: 'SO2', max: 100}
        ],
        shape: 'circle',
        splitNumber: 5,
        name: {
            textStyle: {
                color: 'rgb(238, 197, 102)'
            }
        },
        splitLine: {
            lineStyle: {
                color: [
                    'rgba(238, 197, 102, 0.2)', 'rgba(238, 197, 102, 0.2)',
                    'rgba(238, 197, 102, 0.2)', 'rgba(238, 197, 102, 0.2)',
                    'rgba(238, 197, 102, 0.2)', 'rgba(238, 197, 102, 0.2)'
                ]
            }
        },
        splitArea: {
            show: false
        },
        axisLine: {
            lineStyle: {
                color: 'rgba(238, 197, 102, 0.5)'
            }
        }
    },
    series: [
        {
            name: '北京',
            type: 'radar',
            lineStyle: {
                normal: {
                    width: 1,
                    opacity: 0.5
                }
            },
            data: 'data',
            symbol: 'none',
            itemStyle: {
                color: '#F9713C'
            },
            areaStyle: {
                opacity: 0.1
            }
        },
        {
            name: '上海',
            type: 'radar',
            lineStyle: {
                normal: {
                    width: 1,
                    opacity: 0.5
                }
            },
            data: 'data',
            symbol: 'none',
            itemStyle: {
                color: '#B3E4A1'
            },
            areaStyle: {
                opacity: 0.05
            }
        },
        {
            name: '广州',
            type: 'radar',
            lineStyle: {
                normal: {
                    width: 1,
                    opacity: 0.5
                }
            },
            data: 'data',
            symbol: 'none',
            itemStyle: {
                color: 'rgb(238, 197, 102)'
            },
            areaStyle: {
                opacity: 0.05
            }
        }
    ]
}