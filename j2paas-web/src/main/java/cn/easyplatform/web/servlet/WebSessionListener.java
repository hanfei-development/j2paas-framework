/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.servlet;

import cn.easyplatform.web.Lifecycle;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.util.SessionCleanup;
import org.zkoss.zk.ui.util.SessionInit;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebSessionListener implements SessionInit, SessionCleanup {

    /*
     * (non-Javadoc)
     *
     * @see org.zkoss.web.ui.util.SessionCleanup#cleanup(org.zkoss.web.ui.Session)
     */
    @Override
    public void cleanup(Session sess) throws Exception {
        Lifecycle.endSession(sess);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.zkoss.web.ui.util.SessionInit#init(org.zkoss.web.ui.Session,
     * java.lang.Object)
     */
    @Override
    public void init(Session sess, Object request) throws Exception {
        Lifecycle.beginSession(sess);
    }

}
