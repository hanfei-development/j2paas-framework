/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.util;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Context extends Cloneable {

	Context set(String name, Object value);

	Set<String> keys();

	Context putAll(Object obj);

	Context putAll(String prefix, Object obj);

	boolean has(String key);

	Context clear();

	int size();

	boolean isEmpty();

	Object get(String name);

	Object get(String name, Object dft);

	Object remove(String name);
	
	<T> T getAs(Class<T> type, String name);

	<T> T getAs(Class<T> type, String name, T dft);

	int getInt(String name);

	int getInt(String name, int dft);

	String getString(String name);

	String getString(String name, String dft);

	boolean getBoolean(String name);

	boolean getBoolean(String name, boolean dft);

	float getFloat(String name);

	float getFloat(String name, float dft);

	double getDouble(String name);

	double getDouble(String name, double dft);

	Map<String, Object> getMap(String name);

	List<Object> getList(String name);

	<T> List<T> getList(Class<T> classOfT, String name);

	Context clone();

}