/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zul.A;
import org.zkoss.zul.Vlayout;

import java.util.Iterator;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Movebox extends Vlayout implements ZkExt, Disable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7046023904901323298L;

	/**
	 * 来源列表
	 */
	private String _source;

	/**
	 * 目标列表
	 */
	private String _target;

	/**
	 * 记录映射
	 */
	private String _mapping;

	/**
	 * 图标
	 */
	private String _image;

	/**
	 * 字体图标
	 */
	private String _iconSclass;

	/**
	 * 失效标志
	 */
	private boolean _disabled;

	/**
	 * 显示按钮0,1,1,0
	 */
	private String _display;
	
	/**
	 * 是否唯一，即左边的记录是否可以重复移动
	 */
	private boolean  _unique;
	
	/**
	 * 过滤表达式,target一定存在
	 */
	private String _filter;
	
	public String getFilter() {
		return _filter;
	}

	public void setFilter(String filter) {
		this._filter = filter;
	}
	
	public boolean isUnique() {
		return _unique;
	}

	public void setUnique(boolean unique) {
		this._unique = unique;
	}

	public String getDisplay() {
		return _display;
	}

	public void setDisplay(String display) {
		this._display = display;
	}

	public String getImage() {
		return _image;
	}

	public String getIconSclass() {
		return _iconSclass;
	}

	public void setImage(String image) {
		this._image = image;
	}

	public void setIconSclass(String iconSclass) {
		this._iconSclass = iconSclass;
	}

	public String getSource() {
		return _source;
	}

	public String getTarget() {
		return _target;
	}

	public void setSource(String source) {
		this._source = source;
	}

	public void setTarget(String target) {
		this._target = target;
	}

	public String getMapping() {
		return _mapping;
	}

	public void setMapping(String mapping) {
		this._mapping = mapping;
	}

	@Override
	public boolean isDisabled() {
		return _disabled;
	}

	@Override
	public void setDisabled(boolean disabled) {
		if (this._disabled != disabled) {
			Iterator<Component> itr = queryAll("a").iterator();
			while (itr.hasNext()) {
				A a = (A) itr.next();
				a.setDisabled(disabled);
			}
			this._disabled = disabled;
		}
	}

}
