/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dos;

import cn.easyplatform.type.DeviceType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EnvDo implements Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -5465797895079475058L;

    private String id;

    private DeviceType deviceType;

    private String locale;

    private String portlet;

    private Map<String, Object> variables = new HashMap<String, Object>();

    /**
     * @param deviceType
     * @param locale
     * @param portlet
     * @param variables
     */
    public EnvDo(String id, DeviceType deviceType, String locale,
                 String portlet, Map<String, Object> variables) {
        this.id = id;
        this.deviceType = deviceType;
        this.locale = locale;
        this.portlet = portlet;
        if (variables != null)
            this.variables.putAll(variables);
    }

    public String getId() {
        return id;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public String getLocale() {
        return locale;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the portlet
     */
    public String getPortlet() {
        return portlet;
    }

    /**
     * @return the variables
     */
    public Map<String, Object> getVariables() {
        return variables;
    }

    public EnvDo clone() {
        try {
            return (EnvDo) super.clone();
        } catch (Exception e) {
        }
        return null;
    }
}
