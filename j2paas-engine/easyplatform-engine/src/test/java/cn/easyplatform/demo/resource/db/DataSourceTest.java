/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.resource.db;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataSourceTest extends AbstractDemoTest {

	/**
	 * Demo系统
	 * 
	 * @throws Exception
	 */
	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在清除实体表");
			conn = JdbcTransactions.getConnection(ds1);
			String sql = "truncate table granite_entities_info";
			if (DaoUtils.getDbType(ds1.toString()) == DaoUtils.SYBASE)
				sql = "delete from granite_entities_info";
			pstmt = conn.prepareStatement(sql);
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			log.debug("正在新建demo数据源");
			pstmt = conn
					.prepareStatement("insert into granite_model_info(modelId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, SystemServiceId.SYS_ENTITY_DS);
			pstmt.setString(2, "参数");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATASOURCE.getName());
			String jdbc1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("jdbc1.xml")));
			pstmt.setString(5, jdbc1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();

			pstmt = conn
					.prepareStatement("insert into granite_model_info(modelId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.db.1");
			pstmt.setString(2, "Demo系统业务数据库连接池");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATASOURCE.getName());
			String jdbc2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("jdbc2.xml")));
			pstmt.setString(5, jdbc2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "demo");
			pstmt.setString(8, "C");
			pstmt.execute();
			log.debug("新建demo数据源成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("DataSourceTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
