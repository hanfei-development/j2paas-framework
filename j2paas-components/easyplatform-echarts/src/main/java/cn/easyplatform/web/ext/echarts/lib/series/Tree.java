/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Tree extends Point {

    private String type = "tree";

    private Object width;

    private Object height;

    /**
     * 树图的布局，有 正交 和 径向 两种。这里的 正交布局 就是我们通常所说的 水平 和 垂直 方向，对应的参数取值为 'orthogonal' 。而 径向布局 是指以根节点为圆心，每一层节点为环，一层层向外发散绘制而成的布局，对应的参数取值为 'radial'
     */
    private String layout;
    /**
     * 树图中 正交布局 的方向，也就是说只有在 layout = 'orthogonal' 的时候，该配置项才生效。对应有 水平 方向的 从左到右，从右到左；以及垂直方向的 从上到下，从下到上。取值分别为 'LR' , 'RL', 'TB', 'BT'。注意，之前的配置项值 'horizontal' 等同于 'LR'， 'vertical' 等同于 'TB'
     */
    private String orient;

    /**
     * 标志图形类型，默认自动选择（8种类型循环使用，不显示标志图形可设为'none'）
     */
    private Object symbol;
    /**
     * 标志图形大小，可计算特性启用情况建议增大以提高交互体验。实现气泡图时symbolSize需为Function，气泡大小取决于该方法返回值，传入参数为当前数据项（value数组）
     */
    private Object symbolSize;
    /**
     * 标志图形旋转角度[-180,180]
     */
    private Object symbolRotate;
    /**
     * 标记相对于原本位置的偏移
     */
    private Object symbolOffset;
    /**
     * 如果 symbol 是 path:// 的形式，是否在缩放时保持该图形的长宽比
     */
    private Boolean symbolKeepAspect;
    /**
     * 是否开启鼠标缩放和平移漫游。默认不开启。如果只想要开启缩放或者平移，可以设置成 'scale' 或者 'move'。设置成 true 为都开启
     */
    private Boolean roam;
    /**
     * 子树折叠和展开的交互，默认打开
     */
    private Boolean expandAndCollapse;
    /**
     * 树图初始展开的层级（深度）
     */
    private Integer initialTreeDepth;

    private LineStyle lineStyle;

    private Leavels leavels;
    /**
     * 图形上的文本标签，可用于说明图形的一些数据信息
     */
    private LabelStyle label;
    /**
     * 折线拐点标志的样式
     */
    private ItemStyle itemStyle;
    /**
     * 图形的高亮样式。
     */
    private Emphasis emphasis;

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Tree emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public Tree label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public Tree itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Leavels leavels() {
        if (leavels == null)
            leavels = new Leavels();
        return leavels;
    }

    public Tree leavels(Leavels leavels) {
        this.leavels = leavels;
        return this;
    }

    public Integer initialTreeDepth() {
        return this.initialTreeDepth;
    }

    public Tree initialTreeDepth(Integer initialTreeDepth) {
        this.initialTreeDepth = initialTreeDepth;
        return this;
    }

    public Boolean expandAndCollapse() {
        return this.expandAndCollapse;
    }

    public Tree expandAndCollapse(Boolean expandAndCollapse) {
        this.expandAndCollapse = expandAndCollapse;
        return this;
    }

    public Boolean roam() {
        return this.roam;
    }

    public Tree roam(Boolean roam) {
        this.roam = roam;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return lineStyle;
    }

    public Tree lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public Tree width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Tree height(Object height) {
        this.height = height;
        return this;
    }

    public Object symbol() {
        return this.symbol;
    }

    public Tree symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public Tree symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Object symbolRotate() {
        return this.symbolRotate;
    }

    public Tree symbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public Tree symbolOffset(Object value) {
            this.symbolOffset = value;
        return this;
    }

    public Tree symbolOffset(Object o1, Object o2) {
        this.symbolOffset = new Object[]{o1, o2};
        return this;
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public Tree symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public String getType() {
        return type;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getOrient() {
        return orient;
    }

    public void setOrient(String orient) {
        this.orient = orient;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Object getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }

    public Boolean getRoam() {
        return roam;
    }

    public void setRoam(Boolean roam) {
        this.roam = roam;
    }

    public Boolean getExpandAndCollapse() {
        return expandAndCollapse;
    }

    public void setExpandAndCollapse(Boolean expandAndCollapse) {
        this.expandAndCollapse = expandAndCollapse;
    }

    public Integer getInitialTreeDepth() {
        return initialTreeDepth;
    }

    public void setInitialTreeDepth(Integer initialTreeDepth) {
        this.initialTreeDepth = initialTreeDepth;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Leavels getLeavels() {
        return leavels;
    }

    public void setLeavels(Leavels leavels) {
        this.leavels = leavels;
    }

    public static class Leavels {
        private LabelStyle label;
        private ItemStyle itemStyle;
        private Emphasis emphasis;

        public LabelStyle label() {
            if (label == null)
                this.label = new LabelStyle();
            return this.label;
        }

        public Leavels label(LabelStyle label) {
            this.label = label;
            return this;
        }

        public ItemStyle itemStyle() {
            if (itemStyle == null)
                this.itemStyle = new ItemStyle();
            return this.itemStyle;
        }

        public Leavels itemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
            return this;
        }

        public Emphasis emphasis() {
            if (this.emphasis == null)
                this.emphasis = new Emphasis();
            return this.emphasis;
        }

        public Leavels emphasis(Emphasis emphasis) {
            this.emphasis = emphasis;
            return this;
        }

        public LabelStyle getLabel() {
            return label;
        }

        public void setLabel(LabelStyle label) {
            this.label = label;
        }

        public ItemStyle getItemStyle() {
            return itemStyle;
        }

        public void setItemStyle(ItemStyle itemStyle) {
            this.itemStyle = itemStyle;
        }

        public Emphasis getEmphasis() {
            return emphasis;
        }

        public void setEmphasis(Emphasis emphasis) {
            this.emphasis = emphasis;
        }
    }
}
