/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.support.cell;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.zkex.list.boost.HierarchyBoostBuilder;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Detail;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Label;
import org.zkoss.zul.Span;
import org.zkoss.zul.impl.XulElement;

import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GridCellCreater extends AbstractCellCreater {

    /**
     * @param support
     */
    public GridCellCreater(ListSupport support) {
        super(support);
    }

    private Component createHierarchy(XulElement anchor, Map<String, Component> managedComponents, ListHeaderVo hv, ListRowVo rv, int index) {
        if (hv.isEditable()) {
            Object value = rv.getData()[index];
            if (!Strings.isBlank(hv.getComponent())) {
                if (hv.getInstance() == null) {
                    HtmlBasedComponent newc = createComponent(hv);
                    self = (HtmlBasedComponent) newc.clone();
                    hv.setInstance(newc);
                } else
                    self = (HtmlBasedComponent) ((HtmlBasedComponent) hv.getInstance()).clone();
                self = processComponent(managedComponents, hv, rv, anchor, anchor, self, value);
                if (index == 0 && self instanceof Idspace) {
                    Detail detail = new Detail();
                    detail.appendChild(self);
                    self = detail;
                }
            } else {
                self = PageUtils.createInputComponent(hv.getType());
                bindField(hv, rv, self, anchor, value);
                if (Strings.isBlank(self.getEvent()) && !Strings.isBlank(hv.getEvent()))
                    self.setEvent(hv.getEvent());
                PageUtils.processEventHandler(support, self, anchor);
            }
            if (Strings.isBlank(self.getWidth()))
                self.setHflex("1");
            if (Strings.isBlank(self.getHeight()))
                self.setVflex("1");
            managedComponents.put(hv.getName(), self);
            if (!Strings.isBlank(hv.getCellStyle())) {
                if (self.getHflex() != null) {
                    if (hv.getCellStyle().indexOf("width") >= 0)
                        self.setHflex(null);
                }
                if (self.getVflex() != null) {
                    if (hv.getCellStyle().indexOf("height") >= 0)
                        self.setVflex(null);
                }
                self.setStyle(hv.getCellStyle());
            } else if (hv.getType() == FieldType.INT
                    || hv.getType() == FieldType.LONG
                    || hv.getType() == FieldType.NUMERIC)
                self.setStyle("text-align:right");
            return self;
        } else {
            self = createDetail(anchor, managedComponents, hv, rv, index);
            if (index == 0 && self instanceof Idspace) {
                Detail detail = new Detail();
                detail.appendChild(self);
                self = detail;
            }
            return self;
        }
    }

    @Override
    protected XulElement createCell() {
        return null;
    }

    @Override
    protected void setLabel(XulElement c, String value) {

    }

    @Override
    public Component createCell(XulElement row, Map managedComponents, ListHeaderVo hv, ListRowVo rv, int index) {
        if (!hv.isVisible())
            return new Span();
        if (support instanceof HierarchyBoostBuilder)
            return createHierarchy(row, managedComponents, hv, rv, index);
        return createDetail(row, managedComponents, hv, rv, index);

    }

    private HtmlBasedComponent createDetail(XulElement anchor, Map<String, Component> managedComponents, ListHeaderVo hv, ListRowVo rv, int index) {
        Object value = rv.getData()[index];
        if (!Strings.isBlank(hv.getComponent())) {
            if (hv.getInstance() == null) {
                HtmlBasedComponent newc = createComponent(hv);
                self = (HtmlBasedComponent) newc.clone();
                hv.setInstance(newc);
            } else
                self = (HtmlBasedComponent) ((HtmlBasedComponent) hv.getInstance()).clone();
            self = processComponent(managedComponents, hv, rv, anchor, anchor, self, value);
            managedComponents.put(hv.getName(), self);
        } else {
            Label label = new Label();
            if (value != null) {
                String format = WebUtils
                        .getFormat(support, hv, rv);
                if (!Strings.isBlank(format))
                    label.setValue(WebUtils.format(format, value));
                else
                    label.setValue(value.toString());
            } else
                label.setValue("");
            self = label;
            if (Strings.isBlank(self.getEvent()) && !Strings.isBlank(hv.getEvent()))
                self.setEvent(hv.getEvent());
            PageUtils.processEventHandler(support, self, anchor);
            managedComponents.put(hv.getName(), self);
        }
        if (Strings.isBlank(self.getWidth()))
            self.setHflex("1");
        if (Strings.isBlank(self.getHeight()))
            self.setVflex("1");
        if (!Strings.isBlank(hv.getCellStyle()))
            self.setStyle(hv.getCellStyle());
        else if (hv.getType() == FieldType.INT
                || hv.getType() == FieldType.LONG
                || hv.getType() == FieldType.NUMERIC)
            self.setStyle("text-align:right");
        return self;
    }
}
