/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;

import java.lang.reflect.Array;
import java.util.Collection;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Array2Collection extends Castor<Object, Collection> {

	public Array2Collection() {
		this.fromClass = Array.class;
		this.toClass = Collection.class;
	}

	@Override
	public Collection<?> cast(Object src, Class<?> toType, String... args)
			throws FailToCastObjectException {
		Collection coll = createCollection(src, toType);
		for (int i = 0; i < Array.getLength(src); i++)
			coll.add(Array.get(src, i));
		return coll;

	}
}
