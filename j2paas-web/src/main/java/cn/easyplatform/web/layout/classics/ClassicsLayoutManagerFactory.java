/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.classics;

import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.layout.AbstractLayoutManagerFactory;
import cn.easyplatform.web.layout.IDebugPanelBuilder;
import cn.easyplatform.web.layout.IHomeBuilder;
import cn.easyplatform.web.layout.IMainTaskBuilder;
import cn.easyplatform.web.layout.debug.ClassicsDebugPanelBuilder;
import cn.easyplatform.web.layout.home.DefaultHomeBuilder;
import cn.easyplatform.web.task.impl.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Center;
import org.zkoss.zul.Div;
import org.zkoss.zul.Panelchildren;
import org.zkoss.zul.Tabbox;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ClassicsLayoutManagerFactory extends AbstractLayoutManagerFactory {

    @Override
    public IMainTaskBuilder getMainTaskBuilder(Component container,
                                               String taskId, AbstractPageVo pv) {
        if (pv.getOpenModel() == Constants.OPEN_OVERLAPPED
                || pv.getOpenModel() == Constants.OPEN_MODAL)
            return new DialogMainTaskBuilder(container, taskId, pv);
        if (pv.getOpenModel() == Constants.OPEN_POPUP)
            return new PopupMainTaskBuilder(container, taskId, pv);
        if (pv.getOpenModel() == Constants.OPEN_NORMAL || pv.getOpenModel() == Constants.OPEN_NEW_PAGE) {
            if (Contexts.getEnv().getDeviceType().equals(DeviceType.MOBILE.getName()))
                return new SingletonMainTaskBuilder(container, taskId, pv);
            if (container instanceof Tabbox)
                return new TabpanelMainTaskBuilder(container, taskId, pv);
            else if (container instanceof Center)
                return new SingletonMainTaskBuilder(container, taskId, pv);
            else if (container instanceof Panelchildren)
                return new EmbbedMainTaskBuilder(container, taskId, pv);
            else if (container instanceof Div) {
                if (container.getId().equals("gv5container"))
                    return new PanelMainTaskBuilder(container, taskId, pv);
                return new EmbbedMainTaskBuilder(container, taskId, pv);
            }
            return new DialogMainTaskBuilder(container, taskId, pv);
        }
        if (pv.getOpenModel() > 9)//drawer panel
            return new DrawerMainTaskBuilder(container, taskId, pv);
        if (pv.getOpenModel() == Constants.OPEN_EMBBED || container instanceof Panelchildren)
            return new EmbbedMainTaskBuilder(container, taskId, pv);
        if (container instanceof Tabbox)
            return new TabpanelMainTaskBuilder(container, taskId, pv);
        if (container instanceof Center)
            return new SingletonMainTaskBuilder(container, taskId, pv);
        return new DialogMainTaskBuilder(container, taskId, pv);
    }

    @Override
    public IDebugPanelBuilder getDebugBuilder() {
        return new ClassicsDebugPanelBuilder();
    }

    @Override
    public IHomeBuilder getHomeBuilder() {
        return new DefaultHomeBuilder();
    }

}
