/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.component;

import java.io.Serializable;
import java.util.List;

import cn.easyplatform.type.FieldVo;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListPageVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1182874457516409680L;

	private int totalCount;
	
	private List<FieldVo[]> data;

	/**
	 * @param totalCount
	 * @param data
	 */
	public ListPageVo(int totalCount, List<FieldVo[]> data) {
		this.totalCount = totalCount;
		this.data = data;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}


	/**
	 * @return the data
	 */
	public List<FieldVo[]> getData() {
		return data;
	}
	
}
