/*
 * create by 陈云亮(shiny_vc@163.com)
 */
echarts.ECharts = zk.$extends(zk.Widget, {
    _engine: null,
    _theme: null,
    _options: null,
    _map: null,
    _bmap: null,
    _mapStyle: null,
    _mapbox: null,

    $define: {
        map: function (val) {
        },
        bmap: function (val) {
        },
        mapbox: function (val) {
        },
        mapStyle: function (val) {
        },
        options: function (val) {
            if (this._engine) {
                this._engine.clear();
                this._engine.setOption(jq.evalJSON(val));
            }
        },
        theme: function (val) {
            if (this._engine) {
                this._engine.dispose();
                this._engine = null;
                this._engine = echarts.init(this.$n(), val);
                this._engine.setOption(jq.evalJSON(this._options));
                if(this.isListen('onClick')) {
                    this._engine.on('click', function () {

                    });
                }
            }
        }
    },
    redraw: function (out) {
        out.push('<div ', this.domAttrs_(), '></div>');
    },
    bind_: function () {
        this.$supers(echarts.ECharts, 'bind_', arguments);
        //保证canvas的宽和高大于0
        if (this.$n().clientWidth == 0)
            jq(this.$n()).width("10px");
        if (this.$n().clientHeight == 0)
            jq(this.$n()).height("10px");
        zWatch.listen({onSize: this});
        if (this._bmap) {
            var wgt = this;
            jq.getScript(zk.ajaxURI("/web/js/echarts/ext/extension/bmap.min.js", {au: true}))
                .done(function () {
                    wgt._init();
                    var bmap = wgt._engine.getModel().getComponent('bmap').getBMap();
                    if (wgt._mapStyle)
                        bmap.setMapStyle({styleJson: jq.evalJSON(wgt._mapStyle)});
                    if (wgt._mapbox) //增加地图工具栏
                        bmap.addControl(new BMap.MapTypeControl());
                });
        } else if (this._map && !echarts.getMap(this._map)) {
            var wgt = this;
            jq.getScript(zk.ajaxURI("/web/js/echarts/ext/map/" + this._map + ".js", {au: true}))
                .done(function () {
                    wgt._init();
                });
        } else
            this._init();
    },
    _init: function () {
        this._engine = echarts.init(this.$n(), this._theme);
        var option = jq.evalJSON(this._options);
        this._engine.setOption(option);
    },
    unbind_: function () {
        zWatch.unlisten({onSize: this});
        if (this._engine) {
            this._engine.dispose();
            this._engine = null;
        }
        this.$supers(echarts.ECharts, 'unbind_', arguments);
    },
    onSize: function () {
        this.$supers(echarts.ECharts, 'onSize', arguments);
        if (this._engine && (this._hflex || this._vflex)) {
            var n = this.$n();
            this._engine.resize(n.clientWidth, n.clientHeight, false);
        }
    }
});