/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/11/25 10:31
 * @Modified By:
 */
public class TemplateVo implements Serializable {

    private List<DeviceMapVo> devices;

    private List<PortletVo> portlets;

    public List<DeviceMapVo> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceMapVo> devices) {
        this.devices = devices;
    }

    public List<PortletVo> getPortlets() {
        return portlets;
    }

    public void setPortlets(List<PortletVo> portlets) {
        this.portlets = portlets;
    }
}
