importPackage(java.util, java.lang);

function debug(msg) {
    $.debug(msg);
}

function log(msg) {
    $.log(msg);
}

function alert(msg) {
    $.alert(msg);
}

function sendRedirect() {
    if (arguments.length == 1)
        $.sendRedirect(arguments[0], null);
    else if (arguments.length == 2)
        $.sendRedirect(arguments[0], arguments[1]);
}

function getContextPath() {
    return $.getContextPath();
}

function i18n(name) {
    return $.i18n(name);
}

function isEmpty(value) {
    return $.isEmpty(value);
}

function isEquals(src, target) {
    return $.isEquals(src, target);
}

function toInt(o) {
    return $.toInt(o);
}

function toLong(o) {
    return $.toLong(o);
}

function toDouble(o) {
    return $.toDouble(o);
}

function toStr() {
    if (arguments.length == 1)
        return $.toStr(arguments[0], null);
    else if (arguments.length == 2)
        return $.toStr(arguments[0], arguments[1]);
}

function toDate() {
    if (arguments.length == 1)
        return $.toDate(arguments[0], null);
    else if (arguments.length == 2)
        return $.toDate(arguments[0], arguments[1]);
}

function toDay() {
    if (arguments.length == 0)
        return $.toDay(null);
    else
        return $.toDay(arguments[0]);
}

function format() {
    if (arguments.length == 1)
        return $.format(arguments[0], null);
    else if (arguments.length == 2)
        return $.format(arguments[0], arguments[1]);
}

function getHours(d1, d2) {
    return $.getHours(d1, d2);
}

function getDays(d1, d2) {
    return $.getDays(d1, d2);
}

function getMonths(d1, d2) {
    return $.getMonths(d1, d2);
}

function addDays(d, num) {
    return $.addDays(d, num);
}

function addMonths(d, num) {
    return $.addMonths(d, num);
}

function addYears(d, num) {
    return $.addYears(d, num);
}

function getMaxDayInMonth(year, month) {
    return $.getMaxDayInMonth(year, month);
}

function getRemainingDays(date) {
    return $.getRemainingDays(date);
}

function now() {
    return $.now();
}

function isDefine(name) {
    return $$.containsKey(name);
}

function cache() {
    if (arguments.length == 1)
        return $.cache(arguments[0], null);
    else if (arguments.length == 2)
        return $.cache(arguments[0], arguments[1]);
}

function replace(oldc, newc) {
    if (arguments.length == 2)
        return $.replace(arguments[0], arguments[1]);
    if (arguments.length == 3)
        return $.replace(arguments[0], arguments[1], arguments[2]);
}

function selectObject() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i] = arguments[i];
    return $.evalExpression(arguments[0], 2, args);
}

function selectList() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i] = arguments[i];
    return $.evalExpression(arguments[0], 3, args);
}

function selectMap() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i] = arguments[i];
    return $.evalExpression(arguments[0], 6, args);
}

function setAcc(src, target) {
    $.setAcc(src, target);
}

function toFile(name, data) {
    return $.toFile(name, data);
}

function fromFile(name) {
    return $.fromFile(name);
}

function set(name, value) {
    $.set(name, value);
}

function get(name) {
    return $.get(name);
}

function remove(name) {
    return $.remove(name);
}

function getValue() {
    if (arguments.length == 1)
        return $.getValue(arguments[0]);
    if (arguments.length == 2)
        return $.getValue(arguments[0], arguments[1]);
}

function setValue() {
    if (arguments.length == 2)
        $.setValue(arguments[0], arguments[1]);
    else if (arguments.length == 3)
        $.setValue(arguments[0], arguments[1], arguments[2]);
}

function getObject(name) {
    return $.getObject(name);
}

function getWorkspace() {
    return $.getWorkspace();
}