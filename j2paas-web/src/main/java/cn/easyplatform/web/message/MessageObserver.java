/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message;

import cn.easyplatform.messages.vos.h5.MessageVo;
import cn.easyplatform.spi.listener.event.AppEvent;
import cn.easyplatform.spi.listener.event.ConsoleEvent;
import cn.easyplatform.spi.listener.event.LogEvent;
import cn.easyplatform.spi.listener.event.MessageEvent;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.message.entity.Message;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkmax.ui.util.Toast;

import java.util.Observable;
import java.util.Observer;

import static cn.easyplatform.messages.vos.h5.MessageVo.TYPE_MESSAGE;

/**
 * 系统内置应用侦听者
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageObserver implements Observer, EventListener<Event> {

    private Desktop desktop;

    private String projectId;

    private String userId;

    public MessageObserver(Desktop desktop) {
        this.desktop = desktop;
        if (Contexts.getEnv() != null)
            this.projectId = Contexts.getEnv().getProjectId();
        this.userId = Contexts.getUser().getId();
        if (!desktop.isServerPushEnabled())
            desktop.enableServerPush(true);
        //订阅消息
        Contexts.subscribe(TYPE_MESSAGE, this);
    }

    @Override
    public void update(Observable o, Object arg) {
        Message message = (Message) arg;
        if (this.projectId == null || message.contain(this.projectId, this.userId)) {
            Executions.schedule(desktop, event -> {
                cn.easyplatform.spi.listener.event.Event evt = (cn.easyplatform.spi.listener.event.Event) event.getData();
                if (evt instanceof ConsoleEvent) {
                    Contexts.publish(Message.CONSOLE, evt);
                } else if (evt instanceof MessageEvent) {
                    Contexts.publish(((MessageEvent) evt).getId(), evt.getData());
                } else if (evt instanceof LogEvent) {
                    Contexts.publish(Message.LOG, evt.getData());
                } else if (evt instanceof AppEvent) {
                    AppEvent e = (AppEvent) evt;
                    Contexts.publish(e.getName(), e.getData());
                }
            }, new Event("onMessage", null, message.getEntity()));
        }//if
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getData() instanceof MessageVo) {
            MessageVo mv = (MessageVo) event.getData();
            Toast.show(mv.getFromUser() + ":" + mv.getText(), mv.getType(), "bottom_right", 0, true);
        }
    }
}
