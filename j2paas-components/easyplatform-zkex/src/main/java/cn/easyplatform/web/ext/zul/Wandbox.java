/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zul.Bandbox;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Wandbox extends Bandbox implements ZkExt {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 查询语句,结果column个数可以有1-3个,当1个的时候value和label共用，2个时分别表示value和label，3个时最后一个是图片
     */
    private String _query;

    /**
     * 数据连接的资源id
     */
    private String _dbId;

    /**
     * 排序
     */
    private String _orderBy;

    /**
     * 分页大小
     */
    private int _pageSize = 10;

    /**
     * 过滤表达式一定存在
     */
    private String _filter;

    /**
     * 组成值的前缀,例如%，?可以有多个，以逗号分隔
     */
    private String _prefix;

    /**
     * 组成值的后缀
     */
    private String _suffix;

    /**
     * 列表头
     */
    private String _title;

    /**
     * 打开窗口的宽度
     */
    private String _panelWidth;

    /**
     * 打开窗口的高度
     */
    private String _panelHeight;

    /**
     * 分页组件是否显示详细信息
     */
    private boolean _pagingDetailed = true;

    /**
     * 分页组件显示模式
     */
    private String _pagingMold;

    /**
     * 分页组件显示style
     */
    private String _pagingStyle;

    /**
     * 列头可否调整大小
     */
    private boolean _sizable = true;

    /**
     * 初始逻辑，处理相关的映射
     */
    private String _mapping;

    /**
     * 表示值的栏位
     */
    private String _valueField;

    //位置信息
    private String _offsetX;


    public String getOffsetX() {
        return _offsetX;
    }

    public void setOffsetX(String offsetX) {
        this._offsetX = offsetX;
    }

    public String getValueField() {
        return _valueField;
    }

    public void setValueField(String valueField) {
        if (valueField != null)
            valueField = valueField.toUpperCase();
        this._valueField = valueField;
    }

    public String getMapping() {
        return _mapping;
    }

    public void setMapping(String mapping) {
        this._mapping = mapping;
    }

    /**
     * @return
     */
    public boolean isSizable() {
        return _sizable;
    }

    /**
     * @param sizable
     */
    public void setSizable(boolean sizable) {
        this._sizable = sizable;
    }

    /**
     * @return the pagingMold
     */
    public String getPagingMold() {
        return _pagingMold;
    }

    /**
     * @param pagingMold the pagingMold to set
     */
    public void setPagingMold(String pagingMold) {
        this._pagingMold = pagingMold;
    }

    /**
     * @return the pagingStyle
     */
    public String getPagingStyle() {
        return _pagingStyle;
    }

    /**
     * @param pagingStyle the pagingStyle to set
     */
    public void setPagingStyle(String pagingStyle) {
        this._pagingStyle = pagingStyle;
    }

    /**
     * @return the pagingDetailed
     */
    public boolean isPagingDetailed() {
        return _pagingDetailed;
    }

    /**
     * @param pagingDetailed the pagingDetailed to set
     */
    public void setPagingDetailed(boolean pagingDetailed) {
        this._pagingDetailed = pagingDetailed;
    }

    public String getPanelWidth() {
        return _panelWidth;
    }

    public void setPanelWidth(String panelWidth) {
        this._panelWidth = panelWidth;
    }

    public String getPanelHeight() {
        return _panelHeight;
    }

    public void setPanelHeight(String panelHeight) {
        this._panelHeight = panelHeight;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getPrefix() {
        return _prefix;
    }

    public void setPrefix(String prefix) {
        this._prefix = prefix;
    }

    public String getSuffix() {
        return _suffix;
    }

    public void setSuffix(String suffix) {
        this._suffix = suffix;
    }

    public String getQuery() {
        return _query;
    }

    public void setQuery(String query) {
        this._query = query;
    }

    public String getDbId() {
        return _dbId;
    }

    public void setDbId(String dbId) {
        this._dbId = dbId;
    }

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    public String getOrderBy() {
        return _orderBy;
    }

    public void setOrderBy(String orderBy) {
        this._orderBy = orderBy;
    }

    public int getPageSize() {
        return _pageSize;
    }

    public void setPageSize(int pageSize) {
        this._pageSize = pageSize;
    }

    @Override
    public String getMold() {
        return "wb";
    }

}
