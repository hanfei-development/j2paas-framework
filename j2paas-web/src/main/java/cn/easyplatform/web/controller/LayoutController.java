/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.messages.vos.TaskVo;
import cn.easyplatform.web.task.EventSupport;
import cn.easyplatform.web.task.MainTaskSupport;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface LayoutController {

    /**
     * 跑功能
     *
     * @param obj
     */
    void go(Object obj);

    /**
     * 由事件调用功能
     *
     * @param tv
     * @param parent
     */
    void go(TaskVo tv, EventSupport parent);

    /**
     * 回到上一功能
     */
    void prev();
}
