/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.style.AreaSelectStyle;
import cn.easyplatform.web.ext.echarts.lib.type.AxisType;

/**
 * 平行坐标系
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ParallelAxis extends BaseAxis {
    /**
     * 坐标轴的维度号
     */
    private Integer dim;
    /**
     * 用于定义『坐标轴』对应到哪个『坐标系』中
     */
    private Integer parallelIndex;

    /**
     * 是否坐标轴刷选的时候，实时刷新视图。如果设为 false，则在刷选动作结束时候，才刷新视图。
     */
    private Boolean realtime;

    /**
     * 在坐标轴上可以进行框选，这里是一些框选的设置
     */
    private AreaSelectStyle areaSelectStyle;


    /**
     * 构造函数
     */
    public ParallelAxis() {
        this.type(AxisType.value.toString());
    }

    public Integer dim() {
        return this.dim;
    }

    public ParallelAxis dim(Integer dim) {
        this.dim = dim;
        return this;
    }

    public Boolean realtime() {
        return realtime;
    }

    public ParallelAxis realtime(Boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    public Integer parallelIndex() {
        return this.parallelIndex;
    }

    public ParallelAxis parallelIndex(Integer parallelIndex) {
        this.parallelIndex = parallelIndex;
        return this;
    }

    public Integer getDim() {
        return dim;
    }

    public void setDim(Integer dim) {
        this.dim = dim;
    }

    public Integer getParallelIndex() {
        return parallelIndex;
    }

    public void setParallelIndex(Integer parallelIndex) {
        this.parallelIndex = parallelIndex;
    }

    public AreaSelectStyle areaSelectStyle() {
        if (this.areaSelectStyle == null) {
            this.areaSelectStyle = new AreaSelectStyle();
        }
        return this.areaSelectStyle;
    }

    public ParallelAxis areaSelectStyle(AreaSelectStyle areaSelectStyle) {
        this.areaSelectStyle = areaSelectStyle;
        return this;
    }

    public AreaSelectStyle getAreaSelectStyle() {
        return areaSelectStyle;
    }

    public void setAreaSelectStyle(AreaSelectStyle areaSelectStyle) {
        this.areaSelectStyle = areaSelectStyle;
    }

    public Boolean getRealtime() {
        return realtime;
    }

    public void setRealtime(Boolean realtime) {
        this.realtime = realtime;
    }
}
