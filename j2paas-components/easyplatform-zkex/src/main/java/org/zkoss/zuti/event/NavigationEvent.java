/* NavigationEvent.java

	Purpose:
		
	Description:
		
	History:
		Tue Sep 25 12:12:10 CST 2018, Created by rudyhuang

Copyright (C) 2018 Potix Corporation. All Rights Reserved.
*/
package org.zkoss.zuti.event;

import org.zkoss.zuti.zul.NavigationLevel;
import org.zkoss.zuti.zul.NavigationModel;
import org.zkoss.zk.ui.event.Event;

/**
 * Defines an event that encapsulates changes to a navigation model.
 *
 * @author rudyhuang
 * @since 8.6.0
 */
public class NavigationEvent<T> extends Event {
	public enum Type {
		/** Identifies one data change in the model. */
		CHANGE_DATA,
		/** Identifies one context change in the model. */
		CHANGE_CONTEXT,
		/** Identifies the addition of one item to the model. */
		ADD,
		/** Identifies the removal of one item from the model. */
		REMOVE,
		/** Identifies the current navigated item has changed. */
		NAVIGATE
	}

	private final NavigationModel<T> _model;
	private final NavigationLevel<T> _level;
	private final Type _type;
	private final String _key;
	private final T _current;

	public NavigationEvent(NavigationModel<T> model, NavigationLevel<T> level, Type type, String key, T current) {
		super(type.name(), null, current);
		if (model == null)
			throw new IllegalArgumentException();
		if (level == null)
			throw new IllegalArgumentException();
		this._model = model;
		this._level = level;
		this._type = type;
		this._key = key;
		this._current = current;
	}

	/**
	 * Returns the model that fires this event.
	 */
	public NavigationModel<T> getModel() {
		return _model;
	}

	/**
	 * Returns the level that fires this event.
	 */
	public NavigationLevel<T> getLevel() {
		return _level;
	}

	/**
	 * Returns the event type.
	 */
	public Type getType() {
		return _type;
	}

	/**
	 * Returns the current navigation key.
	 */
	public String getKey() {
		return _key;
	}

	/**
	 * Returns the current navigation data.
	 */
	public T getCurrent() {
		return _current;
	}

	@Override
	public String toString() {
		return "[NavigationEvent type=" + _type + ", level=" + _level + ", key=" + _key + ", current=" + _current + "]";
	}
}
