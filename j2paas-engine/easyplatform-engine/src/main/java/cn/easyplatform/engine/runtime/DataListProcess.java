/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.vos.ListInitVo;
import cn.easyplatform.messages.vos.datalist.*;
import cn.easyplatform.type.ListRowVo;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface DataListProcess {

    /**
     * @param cc
     * @param bean
     * @param iv
     * @return
     */
    ListVo init(CommandContext cc, ListBean bean, ListInitVo iv);

    /**
     * @param cc
     * @param pv
     * @return
     */
    List<ListRowVo> paging(CommandContext cc, ListPagingVo pv);

    /**
     * @param cc
     * @param qv
     * @return
     */
    ListQueryResultVo query(CommandContext cc, ListQueryVo qv);

    /**
     * @param cc
     * @param lc
     * @param from
     * @return
     */
    ListQueryResultVo reload(CommandContext cc, ListContext lc,
                             RecordContext from);

    /**
     * 延时加载
     *
     * @param cc
     * @param lc
     * @param from
     * @return
     */
    Object load(CommandContext cc, ListContext lc,
                ListLoadVo from);
}
