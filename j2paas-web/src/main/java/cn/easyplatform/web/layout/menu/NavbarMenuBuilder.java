/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.*;
import cn.easyplatform.web.layout.IMenuBuilder;
import cn.easyplatform.web.listener.RunTaskListener;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Nav;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class NavbarMenuBuilder implements IMenuBuilder {

    private Navbar navbar;

    private Component container;

    public NavbarMenuBuilder(Navbar navbar) {
        this.navbar = navbar;
    }

    @Override
    public void fill(AuthorizationVo av) {
        if (av == null || av.getRoles() == null)
            return;
        this.container = navbar.getFellow("gv5container");
        List<String> temp = new ArrayList<>();
        for (RoleVo rv : av.getRoles())
            createNavbar(temp, rv, null);
        if (av.getAgents() != null && !av.getAgents().isEmpty()) {
            for (AgentVo agent : av.getAgents()) {
                for (RoleVo rv : agent.getRoles())
                    createNavbar(temp, rv, agent);
            }
        }
        temp = null;
        //open第1个菜单
        if (navbar.getFirstChild() != null && navbar.getFirstChild() instanceof Nav)
            ((Nav) navbar.getFirstChild()).setOpen(true);
    }

    private void createNavbar(List<String> temp, RoleVo rv, AgentVo av) {
        for (MenuVo mv : rv.getMenus()) {
            if (!temp.contains(mv.getId())) {
                createNav(navbar, mv, rv, av, 0);
                temp.add(mv.getId());
            }
        }
    }

    private void createNav(Component parent, MenuVo mv, RoleVo rv, AgentVo av,
                           int level) {
        Nav nav = new Nav(mv.getName());
        nav.setAttribute("id", mv.getId());
        if (!Strings.isBlank(mv.getImage()))
            PageUtils.setTaskIcon(nav, mv.getImage());
        else
            nav.setIconSclass(ExtUtils.getIconSclass());
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            for (MenuVo cv : mv.getChildMenus())
                createNav(nav, cv, rv, av, level + 1);
        }
        if (mv.getTasks() != null && !mv.getTasks().isEmpty())
            createTask(nav, mv.getTasks(), rv, av, level);
        //navbar第二层没有缩进
        if (level == 1)
            nav.setSclass("pl-3");
        nav.setParent(parent);
    }

    private void createTask(Component parent, List<MenuNodeVo> tasks,
                            RoleVo rv, AgentVo av, int level) {
        for (MenuNodeVo tv : tasks) {
            tv.setRoleId(rv.getId());
            if (av != null)
                tv.setAgent(av.getId());
            Navitem nv = new Navitem();
            nv.setAttribute("id", tv.getId());
            nv.setLabel(tv.getName());
            nv.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv
                    .getDesp());
            if (level == 0)
                nv.setSclass("pl-3");
            PageUtils.setTaskIcon(nv, tv.getImage());
            nv.setParent(parent);
            nv.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, container));
        }
    }
}
