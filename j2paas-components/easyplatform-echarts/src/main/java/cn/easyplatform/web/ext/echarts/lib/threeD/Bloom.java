/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class Bloom implements Serializable {
    /**
     * 是否开启光晕特效。
     */
    private boolean enable;
    /**
     * 光晕的强度，默认为 0.1
     */
    private int bloomIntensity;

    public Boolean enable() {
        return this.enable;
    }
    public Bloom enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public Integer bloomIntensity() {
        return this.bloomIntensity;
    }
    public Bloom bloomIntensity(Integer bloomIntensity) {
        this.bloomIntensity = bloomIntensity;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getBloomIntensity() {
        return bloomIntensity;
    }

    public void setBloomIntensity(int bloomIntensity) {
        this.bloomIntensity = bloomIntensity;
    }
}
