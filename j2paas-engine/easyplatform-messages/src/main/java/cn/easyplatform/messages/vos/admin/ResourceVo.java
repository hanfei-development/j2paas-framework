/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResourceVo implements Serializable {

    private String id;

    private String type;

    private String subType;

    private String name;

    private int state;

    private Date startTime;

    private Map<String, String> props;

    private Map<String, Object> runtimeInfo;

    /**
     * @param props
     */
    public ResourceVo(String id, String name, Map<String, String> props) {
        this.id = id;
        this.name = name;
        this.props = props;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the subType
     */
    public String getSubType() {
        return subType;
    }

    /**
     * @param subType the subType to set
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * @return the runtimeInfo
     */
    public Map<String, Object> getRuntimeInfo() {
        return runtimeInfo;
    }

    /**
     * @param runtimeInfo the runtimeInfo to set
     */
    public void setRuntimeInfo(Map<String, Object> runtimeInfo) {
        this.runtimeInfo = runtimeInfo;
    }

    /**
     * @return the props
     */
    public Map<String, String> getProps() {
        return props;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
