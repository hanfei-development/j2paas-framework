<vlayout hflex="1" vflex="1" xmlns:n="native" apply="cn.easyplatform.web.controller.admin.HomeController">
    <n:div class="container-fluid">
        <n:div class="row">
            <n:div class="col-12 col-md-6 col-lg-3 p-1">
                <n:div class="state-box rounded p-1 primary">
                    <n:div class="row">
                        <n:div class="col-8 px-4">
                            <label sclass="h6 d-block" value="${c:l('admin.user.nums')}"/>
                            <label sclass="h4 d-block py-2" id="userCount"/>
                            <label sclass="d-block text-success" id="userRate"/>
                        </n:div>
                        <n:div zclass="col-4 text-right px-4">
                            <n:h2>
                                <n:i class="z-icon-group h3"/>
                            </n:h2>
                        </n:div>
                    </n:div>
                </n:div>
            </n:div>
            <n:div class="col-12 col-md-6 col-lg-3 p-1">
                <n:div class="state-box rounded p-1 warning">
                    <n:div class="row">
                        <n:div class="col-8 px-4">
                            <label sclass="h6 d-block" value="${c:l('admin.home.db.pools')}"/>
                            <label sclass="h4 d-block py-2" id="dbCount"/>
                            <label sclass="d-block text-success" id="dbRate"/>
                        </n:div>
                        <n:div class="col-4 text-right px-4">
                            <n:h2>
                                <a iconSclass="z-icon-list h3"/>
                            </n:h2>
                        </n:div>
                    </n:div>
                </n:div>
            </n:div>
            <n:div class="col-12 col-md-6 col-lg-3 p-1">
                <n:div class="state-box rounded p-1 danger">
                    <n:div class="row">
                        <n:div class="col-8 px-4">
                            <label sclass="h6 d-block" value="${c:l('admin.home.disk')}"/>
                            <label sclass="h4 d-block py-2" id="diskCount"/>
                            <label sclass="d-block text-success" id="diskRate"/>
                        </n:div>
                        <n:div class="col-4 text-right px-4">
                            <n:h2>
                                <n:i class="z-icon-tasks h3"/>
                            </n:h2>
                        </n:div>
                    </n:div>
                </n:div>
            </n:div>
            <n:div class="col-12 col-md-6 col-lg-3 p-1">
                <n:div class="state-box rounded p-1 info">
                    <n:div class="row">
                        <n:div class="col-8 px-4">
                            <label sclass="h6 d-block" value="${c:l('admin.home.core.pools')}"/>
                            <label sclass="h4 d-block py-2" id="coreCount"/>
                            <label sclass="d-block text-success" id="coreRate"/>
                        </n:div>
                        <n:div class="col-4 text-right px-4">
                            <n:h2>
                                <n:i class="z-icon-gears h3"/>
                            </n:h2>
                        </n:div>
                    </n:div>
                </n:div>
            </n:div>
        </n:div>
    </n:div>
    <panel title="${c:l('admin.home.panel')}" border="normal" hflex="1" collapsible="true" height="170px">
        <panelchildren>
            <charts id="gaugeChart" type="gauge" plotBackgroundImage="" height="145"
                    title="" backgroundColor="transparent" hflex="1"/>
        </panelchildren>
    </panel>
    <hlayout hflex="1" vflex="1">
        <panel title="${c:l('admin.home.processor')}" border="normal" hflex="1" vflex="1" collapsible="true">
            <panelchildren>
                <charts id="cpuChart" title="" marginRight="10" backgroundColor="transparent" hflex="1"
                        vflex="1"/>
            </panelchildren>
        </panel>
        <panel title="${c:l('admin.home.memory')}" border="normal" hflex="1" vflex="1" collapsible="true">
            <panelchildren>
                <charts id="memoryChart" title="" marginRight="10" backgroundColor="transparent"
                        hflex="1" vflex="1"/>
            </panelchildren>
        </panel>
    </hlayout>
    <panel title="${c:l('admin.project.runtime')}" border="normal" hflex="1" vflex="1" sclass="mr-1" collapsible="true">
        <panelchildren>
            <grid fixedLayout="true" hflex="1" vflex="1">
                <columns sizable="true">
                    <column label="${c:l('admin.jvm.type')}" width="120px"/>
                    <column label="${c:l('admin.jvm.value')}" hflex="1"/>
                </columns>
                <rows>
                    <row>
                        <label value="EP${c:l('admin.jvm.version')}"/>
                        <label id="epVersion"/>
                    </row>
                    <row>
                        <label value="VM${c:l('admin.jvm.version')}"/>
                        <label id="vmVersion"/>
                    </row>
                    <row>
                        <label value="VM${c:l('admin.jvm.vendor')}"/>
                        <label id="vmVendor"/>
                    </row>
                    <row>
                        <label value="VM${c:l('admin.jvm.args')}"/>
                        <label id="vmArgs"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.os')}"/>
                        <label id="osName"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.arch')}"/>
                        <label id="osArch"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.processors')}"/>
                        <label id="processors"/>
                    </row>
                    <row>
                        <label value="PID"/>
                        <label id="pid"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.total.memory')}"/>
                        <label id="totalMemory"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.start.time')}"/>
                        <label id="startTime"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.up.time')}"/>
                        <label id="upTime"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.command')}"/>
                        <label id="vmCommand"/>
                    </row>
                    <row>
                        <label value="${c:l('admin.jvm.total.threads')}"/>
                        <label id="totalThread"/>
                    </row>
                </rows>
            </grid>
        </panelchildren>
    </panel>
</vlayout>