/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.api;

import cn.easyplatform.engine.cmd.identity.ApiInitCmd;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.Command;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ApiInitRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/29 11:02
 * @Modified By:
 */
public class AuthCmd extends AbstractCommand<ApiInitRequestMessage> {

    public AuthCmd(ApiInitRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        Command cmd = new ApiInitCmd(req);
        IResponseMessage<?> resp = cmd.execute(cc);
        if (!resp.isSuccess())
            return resp;
        Subject currentUser = SecurityUtils.getSubject();
        int timeout = cc.getProjectService().getConfig().getAsInt("jwt.timeout", cc.getProjectService().getConfig().getSessionTimeout());
        currentUser.getSession().setTimeout(timeout * 1000);
        Map<String, Object> map = new HashMap<>(2);
        map.put("timeout", timeout);
        map.put("sessionKey", currentUser.getSession().getId());
        if(resp.getBody() != null)
            map.put("roles", resp.getBody());
        return new SimpleResponseMessage(map);
    }
}
