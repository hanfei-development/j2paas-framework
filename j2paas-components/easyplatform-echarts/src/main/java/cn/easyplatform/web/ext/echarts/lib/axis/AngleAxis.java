/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AngleAxis extends BaseAxis {
    /**
     * 用于定义『坐标轴』对应到哪个『坐标系』中
     */
    private Integer parallelIndex;
    /**
     * 起始刻度的角度，默认为 90 度，即圆心的正上方。0 度为圆心的正右方
     */
    private Integer startAngle;
    /**
     * 刻度增长是否按顺时针，默认顺时针
     */
    private Boolean clockwise;

    public Boolean clockwise() {
        return clockwise;
    }

    public AngleAxis clockwise(Boolean clockwise) {
        this.clockwise = clockwise;
        return this;
    }

    public Integer parallelIndex() {
        return this.parallelIndex;
    }

    public AngleAxis parallelIndex(Integer parallelIndex) {
        this.parallelIndex = parallelIndex;
        return this;
    }

    public Integer startAngle() {
        return this.startAngle;
    }

    public AngleAxis startAngle(Integer startAngle) {
        this.startAngle = startAngle;
        return this;
    }

    public Integer getParallelIndex() {
        return parallelIndex;
    }

    public void setParallelIndex(Integer parallelIndex) {
        this.parallelIndex = parallelIndex;
    }

    public Integer getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(Integer startAngle) {
        this.startAngle = startAngle;
    }

    public Boolean getClockwise() {
        return clockwise;
    }

    public void setClockwise(Boolean clockwise) {
        this.clockwise = clockwise;
    }
}
