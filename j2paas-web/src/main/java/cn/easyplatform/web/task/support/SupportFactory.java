/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.support;

import cn.easyplatform.web.task.EventSupport;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.support.cell.GridCellCreater;
import cn.easyplatform.web.task.support.cell.ListCellCreater;
import cn.easyplatform.web.task.support.cell.TreeCellCreater;
import cn.easyplatform.web.task.support.scripting.CompliableScriptEngine;
import cn.easyplatform.web.task.support.scripting.EventScriptEngine;
import cn.easyplatform.web.task.support.scripting.SimpleCompliableScriptEngine;
import cn.easyplatform.web.task.support.scripting.SimpleExpressionEngine;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Tree;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class SupportFactory {

	private SupportFactory() {
	}

	public static final ExpressionEngine getExpressionEngine(
			EventSupport main, String expression) {
		return new SimpleExpressionEngine(main, expression);
	}

	public static final ScriptEngine getScriptEngine() {
		return new EventScriptEngine();
	}

	public final static CompliableScriptEngine getCompliableScriptEngine(
			EventSupport handler) {
		return new SimpleCompliableScriptEngine(handler);
	}

	public final static CellCreater getCellCreater(ListSupport support) {
		if (support.getComponent() instanceof Listbox)
			return new ListCellCreater(support);
		else if (support.getComponent() instanceof Tree)
			return new TreeCellCreater(support);
		else
			return new GridCellCreater(support);
	}
}
